﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 22.10.2018
 * Time: 23:04
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace NetClient
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");
			try
            {
                SendMessageFromSocket(12345);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }
        static void SendMessageFromSocket(int port)
        {
            byte[] bytes = new byte[1024];
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = IPAddress.Parse("127.0.0.1");
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);
            Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            sender.Connect(ipEndPoint);
            Console.Write("input message: ");
            string message = Console.ReadLine();
            Console.WriteLine("Strarting conection to {0} ", sender.RemoteEndPoint.ToString());
            byte[] msg = Encoding.UTF8.GetBytes(message);
            int bytesSent = sender.Send(msg);
            int bytesRec = sender.Receive(bytes);
            Console.WriteLine("\nResponse: {0}\n\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));
            if (message.IndexOf("<TheEnd>") == -1)
                SendMessageFromSocket(port);
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
            }
      }
}